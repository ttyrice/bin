#!/bin/sh

file="$HOME/todo.txt"
touch "$file"
height=$(( 3 + $(wc -l "$file" | cut -d" " -f1) ))
prompt="Add/delete a task: "

cmd=$(fzy -l "$height" -p "$prompt" < "$file")
while [ -n "$cmd" ]; do
	if grep -q "^$cmd\$" "$file"; then
		grep -v "^$cmd\$" "$file" > "$file.$$"
		mv "$file.$$" "$file"
		height=$(( height-1 ))
	else
		echo "$cmd" >> "$file"
		height=$(( height+1 ))
	fi

	cmd=$(fzy -l "$height" -p "$prompt" < "$file")
done

exit 0
