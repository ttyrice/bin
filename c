#!/bin/sh

# if data on stdin -> store .clip; otherwise -> print .clip
# edit: vis fucks it up so we'll print every time

if [ -t 0 ]; then
	cat "$HOME/.clip"
else
	cat "$HOME/.clip"
	> "$HOME/.clip"
	while read -r line; do
		echo "$line" >> "$HOME/.clip"
	done
fi
